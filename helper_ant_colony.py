from libs import ant_colony as ac
import networkx as nx
from time import time


def prepare_for_ant(G):
    g_dict = nx.to_dict_of_dicts(G)
    for num in g_dict:
        lt = []
        for i in g_dict[num]:
            lt.append(i)
        g_dict[num] = tuple(lt)
    return G, g_dict


def add_last_node(solution):
    solution.append(solution[0])
    return solution


def calculate_cost(G, solution):
    cost = 0
    prev_node = -1
    for i in solution:
        if prev_node == -1:
            prev_node = i
        else:
            cost += G[prev_node][i]['weight']
            prev_node = i
    return cost


def run(G, part_g, start=None, ant_count=50, alpha=.5, beta=1.2, pheromone_evaporation_coefficient=.40, pheromone_constant=1000.0, iterations=80):
    start_time = time()
    pr_g, nodes = prepare_for_ant(part_g)

    def return_weight(start, end):
        n1 = nodes.keys()[nodes.values().index(start)]
        n2 = nodes.keys()[nodes.values().index(end)]
        return G[n1][n2]['weight']

    colony = ac.ant_colony(nodes, return_weight, start=start, ant_count=ant_count, alpha=alpha, beta=beta,
                 pheromone_evaporation_coefficient=pheromone_evaporation_coefficient, pheromone_constant=pheromone_constant, iterations=iterations)

    answer = add_last_node(colony.mainloop())
    cost = calculate_cost(G, answer)
    stop_time = time()
    return {'solution': answer, 'time': stop_time - start_time, 'cost': cost}
