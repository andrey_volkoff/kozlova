# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.
import random
from random import randrange


class Bee_colony(object):
    def __init__(self, vrt, individuals_length, decode, fitness):
        self.vrt = vrt
        self.individuals_length = individuals_length
        self.decode = decode
        self.fitness = fitness


def decodeTSP(path, cities):
    lista = []
    for i in path:
        lista.append(cities.get(i))
    return lista


def fitnessTSP(path, distances):
    def distanceTrip(index, city):
        w = distances.get(index)
        return w[city]

    actualpath = list(path)
    fitness_value = 0
    count = 0

    for i in path:
        if count + 1 < (len(path)):
            temp = count + 1
            nextCity = actualpath[temp]

            fitness_value += distanceTrip(i, nextCity)
            count += 1

    return fitness_value


# k - number of elite paths to pick
# steps - number of generations
# size - size of "population"
# start - start point
# dist_bases - max radius of base searching
# q - number of bases per each elite path
def bee_algorithm(Bee_colony, k, steps, size, start, dist_bases, q, distances):
    def fitnessTSP_new(path):
        def distanceTrip(index, city):
            w = distances.get(index)
            return w[city]

        actualpath = list(path)
        fitness_value = 0
        count = 0

        for i in path:
            if count + 1 < (len(path)):
                temp = count + 1
                nextCity = actualpath[temp]

                fitness_value += distanceTrip(i, nextCity)
                count += 1

        return fitness_value

    def initial_population(Bee_colony, size):
        def generate_path():
            path = []
            for i in Bee_colony.vrt:
                path.append(i)
            random.shuffle(path)
            b = path.index(start)
            path[b], path[0] = path[0], path[b]
            path.append(start)
            return path

        return [generate_path() for _ in list(range(size))]

    def new_generation_t(Bee_colony, k, population, dist_bases, q):

        def find_best(Bee_colony, population, k):

            winners = []
            elements = population
            elements = sorted(elements, key=fitnessTSP_new)
            for i in range(int(len(elements) - 1), int(len(elements) - k + 1), -1):
                now = elements[i]
                winners.append(now)

            return winners

        def find_bases(path, prob, q):

            if prob > (len(path)):
                while prob > len(path):
                    prob - len(path)

            def inversion_search(path_aux):
                path = path_aux
                index1 = randrange(1, prob)
                index2 = randrange(index1, prob)
                path_mid = path[index1:index2]
                path_mid.reverse()
                path_result = path[0:index1] + path_mid + path[index2:]

                return path_result

            def swap_search(path_aux):
                path = path_aux
                for i in range(prob):
                    pos = random.randint(1, len(path) - 3)
                    path[pos], path[pos + 1] = path[pos + 1], path[pos]
                return path

            aux = []
            for i in range(q):
                aux.append(swap_search(path))
            return aux

        best = find_best(Bee_colony, population, k)

        bases = []
        for i in range(len(best) - 1):
            pop = best[i]
            bases = bases + find_bases(pop, dist_bases, q)

        if k + q * k < size / 2:
            best_bases = bases
        else:
            best_bases = find_best(Bee_colony, bases, size / 2)

        new_generation = initial_population(Bee_colony, (size // 2)) + best_bases

        return new_generation

    population = initial_population(Bee_colony, size)

    for _ in range(steps):
        population = new_generation_t(Bee_colony, k, population, dist_bases, q)

    bestChromosome = min(population, key=Bee_colony.fitness)
    genotype = Bee_colony.decode(bestChromosome)
    return (genotype, Bee_colony.fitness(bestChromosome))


def TSP(k, cities, distances, steps=20, size=20, start=0, dist_bases=3, q=3):
    TSP_PROBLEM = Bee_colony(list(cities), len(cities), lambda x: decodeTSP(x, cities), lambda y: fitnessTSP(y, distances))
    return bee_algorithm(TSP_PROBLEM, k, steps, size, start, dist_bases, q, distances)

