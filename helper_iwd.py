import libs.IWD.iwd as iwd
from time import time


def prepare_graph(full_g):
    distance_matrix = {}
    for i in full_g.nodes():
        distance_matrix[i] = {}
        for j in full_g.nodes():
            if i == j:
                distance_matrix[i][j] = 0
            else:
                distance_matrix[i][j] = full_g.get_edge_data(i, j)['weight']

    return distance_matrix


def add_last_node(G, solution, cost):
    prev_last_node = solution[-1]
    solution.append(solution[0])
    last_node = solution[-1]
    cost += G[prev_last_node][last_node]['weight']
    return solution, cost


def run(full_g, soil_parameters=[1, 0.01, 1], a_s=1, b_s=0.01, c_s=1, velocity_parameters=[1, 0.01, 1], a_v=1, b_v=0.01,
        c_v=1, maximum_iterations=1000, iteration_count=0, initial_amount_of_soil=1000, p_n=0.9, p_iwd=0.9):

    start_time = time()
    parameters = iwd.parameters_initialization()
    distance_matrix = prepare_graph(full_g)

    parameters.initialize_graph(len(full_g.nodes()), distance_matrix)

    parameters.parameter_list['soil_parameters'] = soil_parameters
    parameters.parameter_list['soil_parameters'][0] = a_s
    parameters.parameter_list['soil_parameters'][1] = b_s
    parameters.parameter_list['soil_parameters'][2] = c_s
    parameters.parameter_list['velocity_parameters'] = velocity_parameters
    parameters.parameter_list['velocity_parameters'][0] = a_v
    parameters.parameter_list['velocity_parameters'][1] = b_v
    parameters.parameter_list['velocity_parameters'][2] = c_v
    parameters.parameter_list['maximum_iterations'] = maximum_iterations
    parameters.parameter_list['iteration_count'] = iteration_count
    parameters.parameter_list['initial_amount_of_soil'] = initial_amount_of_soil
    parameters.parameter_list['p_n'] = p_n
    parameters.parameter_list['p_iwd'] = p_iwd

    solution = iwd.compute(parameters.parameter_list)
    add_last_node(full_g, solution[0], solution[2])
    stop_time = time()
    return {'time': stop_time - start_time, 'solution': solution[0], 'cost': solution[2], 'quality': solution[1]}
