import libs.genetic as gen_al


def prepare_for_genetic(full_g, part_g):
    nodes_list = part_g.nodes()
    nodes_dict = {}
    num = 0
    for i in nodes_list:
        nodes_dict[num] = i
        num += 1

    distances = {}
    num = 0
    for i in nodes_list:
        values = []
        for j in nodes_list:
            if i == j:
                values.append(full_g.get_edge_data(i, j))
            else:
                values.append(full_g.get_edge_data(i, j)['weight'])
        distances[num] = values
        num += 1
    return nodes_dict, distances


def run(full_g, part_g, count=2, opt=min, ngen=200, size=100, ratio_cross=0.8, prob_mutate=0.05):
    genetic_problem_instances = 0
    nodes_dict, distances = prepare_for_genetic(full_g, part_g)
    return gen_al.TSP(genetic_problem_instances, nodes_dict, distances, count, opt, ngen, size, ratio_cross, prob_mutate)
