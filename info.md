Info about parameters of :

- Ant colony
    - `start` - if set, then is assumed to be the node where all ants start their traversal 
    - `ant_count` - number of ants
    - `alpha` - parameter from the ACO algorithm to control the influence of the amount of pheromone when an ant makes a choice
    - `beta` - parameters from ACO that controls the influence of the distance to the next node in ant choice making
    - `pheromone_evaporation_coefficient` - parameter used in removing pheromone values from the pheromone_map (rho in ACO algorithm)
    - `pheromone_constant` - parameter used in depositing pheromones on the map (Q in ACO algorithm)
    - `iterations` - how many iterations to let the ants traverse the map
- Bee colony
    - `k` - number of elitist pathes
    - `steps` - number of generations (search iterations)
    - `size` - population size
    - `start` - start node
    - `dist_bases` - maximum search radius
    - `q` - number of bases for each elitist node
- IWD and MIWD
    - `soil_parameters` - soil comprise
        - a_s
        - b_s
        - c_s
    - `velocity_parameters` - velocity comprise 
        - a_v
        - b_v
        - c_v
    - maximum_iterations
    - iteration_count
    - initial_amount_of_soil
    - p_n
    - p_iwd
- Genetic
    - `count` - number of participants on the selection tournaments
    - `opt` - max or min, indicating if it is a maximization or a minimization problem.
    - `ngen` - number of generations (halting condition)
    - `size` - number of individuals for each generation
    - `ratio_cross` - portion of the population which will be obtained by means of crossovers
    - `prob_mutate` - probability that a gene mutation will take place
- Particle swarm optimization
    - `iterations` - number of iterations
    - `size_population` - population size
    - `beta` - the probability that all swap operators in swap sequence (gbest - x(t-1))
    - `alfa` - the probability that all swap operators in swap sequence (pbest - x(t-1))


1) Static parameter initialization
        a) Problem representation in the form of a graph
        b) Setting values for static parameters
2) Dynamic parameter initialization: soil and velocity of IWDs
3) Distribution of IWDs on the problem’s graph
4) Solution construction by IWDs along with soil and velocity updating
        a) Local soil updating on the graph
        b) Soil and velocity updating on the IWDs
5) Local search over each IWD’s solution (optional)
6) Global soil updating
7) Total-best solution updating
8) Go to step 2 unless termination condition is satisfied
