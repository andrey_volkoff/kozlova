import networkx as nx

import libs.maxcutpy.graphcut as gc
import libs.maxcutpy.maxcut as mc


def divide_graph(G):
    cuts = mc.trevisan_approximation_alg(G)

    blue_nodes, black_nodes, undecided_nodes, marked_nodes = gc.get_partitions(G)

    cut_graph_blue = nx.Graph()
    cut_graph_blue.add_nodes_from(blue_nodes)
    cut_graph_blue.add_edges_from(list(nx.edge_boundary(G, blue_nodes, blue_nodes)))

    cut_graph_black = nx.Graph()
    cut_graph_black.add_nodes_from(black_nodes)
    cut_graph_black.add_edges_from(list(nx.edge_boundary(G, black_nodes, black_nodes)))

    return cuts, cut_graph_black, cut_graph_blue


def divide_graph_recursively(G, parts):
    curr_graphs = list()
    next_graphs = list()

    parts = parts - 1
    curr_graphs.append(G)

    while parts >= len(curr_graphs) and parts > 0:
        if len(curr_graphs) > 0:
            parts = parts - 1
            cuts, graph1, graph2 = divide_graph(curr_graphs.pop())
            next_graphs.append(graph1)
            next_graphs.append(graph2)
        else:
            curr_graphs = next_graphs
            next_graphs = list()

    if len(curr_graphs) == 0 and parts == 0:
        return next_graphs
    elif len(next_graphs) == 0:
        graphs_copy = list(curr_graphs)
        cuts_list = list()
        graph_list1 = list()
        graph_list2 = list()
        allotted_graphs = list()
        while len(graphs_copy) > 0:
            cuts, graph1, graph2 = divide_graph(graphs_copy.pop())
            cuts_list.append(cuts)
            graph_list1.append(graph1)
            graph_list2.append(graph2)

        while parts > 0:
            num = cuts_list.index(max(cuts_list))
            curr_graphs.pop(num)
            cuts_list.pop(num)
            allotted_graphs.append(graph_list1.pop(num))
            allotted_graphs.append(graph_list2.pop(num))
            parts = parts - 1

        curr_graphs.extend(allotted_graphs)
        return curr_graphs
