Branches:
-
- `master` - for python v2.7
- `v_3_6` - for python v3.6


Used libraries:
-
- https://github.com/bluesurfer/maxCutPy
- https://github.com/fermenreq/TSP-VRP-GENETICS-ALGORITHM
- https://github.com/pjmattingly/ant-colony-optimization
- https://github.com/marcoscastro/tsp_pso
- http://pythonhosted.org/IWD/

All this libs was modified and locate in /libs.

Dependencies:
-
- Networkx 2.0
- Numpy
- Scipy
- matplotlib
- argparse

Run
-
Use `python 2.7` and run `app.py`.

Structure:
-
Project contains helpers for each algorithm and for max cut.
This helpers have functions `run` as entry point (call it to use algorithm).
- helper_ant_colony
- helper_genetic
- helper_pso
- helper_iwd
- helper_miwd

Max cut helper has function `divide_graph_recursively` as entry point.


Parameters:
-
- `divide_graph_recursively(G, parts)`, where `G` - networkx graph, 
`parts` - number of subgraphs.
- `helper_ant_colony.run`, `helper_genetic.run` and `helper_pso.run` 
receives full graph with weights as first argument(`full_g`) and subgraph with 
only edges and nodes - as second(`part_g`). Also each `run` function receives 
parameters of algorithms (See helpers for details).