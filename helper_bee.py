import libs.beegees as bee_al
from time import time


def prepare_for_genetic(full_g, part_g):
    nodes_list = part_g.nodes()
    nodes_dict = {}
    num = 0
    for i in nodes_list:
        nodes_dict[num] = i
        num += 1

    distances = {}
    num = 0
    for i in nodes_list:
        values = []
        for j in nodes_list:
            if i == j:
                values.append(full_g.get_edge_data(i, j))
            else:
                values.append(full_g.get_edge_data(i, j)['weight'])
        distances[num] = values
        num += 1
    return nodes_dict, distances


def run(full_g, part_g, k=5, steps=20, size=20, start=0, dist_bases=3, q=3):
    start_time = time()
    nodes_dict, distances = prepare_for_genetic(full_g, part_g)
    result = bee_al.TSP(k, nodes_dict, distances, steps, size, start, dist_bases, q)
    stop_time = time()
    return {'solution': result[0], 'cost': result[1], 'time': stop_time - start_time}
