import networkx as nx

import matplotlib.pylab as plt
import libs.maxcutpy.graphdraw as gd

import helper_max_cut as hmc
import helper_ant_colony as hac
import helper_genetic as hgn
import helper_pso as hpo
import helper_iwd as hiw
import helper_miwd as hmw
import helper_bee as hbe


def run(G, parts):
    if parts > len(G.nodes()):
        raise Exception('Nodes must be greater than parts')

    graphs = hmc.divide_graph_recursively(G, parts)

    for i in graphs:
        result_ant = hac.run(G, i)
        result_genetic = hgn.run(G, i)
        result_pso = hpo.run(G, i)

        print(result_ant)
        print(result_pso)
        print(result_genetic)

        # gd.draw_custom(i)
        # plt.show()


def prepare_weighted_graph(G):
    from random import randint

    new_g = nx.Graph()
    edges = G.edges()
    for i in edges:
        new_g.add_edge(i[0], i[1], weight=randint(1, 8))
    return new_g


if __name__ == '__main__':
    G = prepare_weighted_graph(nx.complete_graph(9))

    result_bee = hbe.run(G, G, k=5, steps=20, size=20, start=0, dist_bases=3, q=3)
    result_ant = hac.run(G, G, start=0, ant_count=50, alpha=.5, beta=1.2, pheromone_evaporation_coefficient=.40,
                         pheromone_constant=1000.0, iterations=80)
    result_genetic, result_mod_genetic = hgn.run(G, G, count=2, opt=min, ngen=200, size=100, ratio_cross=0.8,
                                                 prob_mutate=0.05)
    result_pso = hpo.run(G, G, iterations=100, size_population=10, beta=1, alfa=0.9)
    result_iwd = hiw.run(G, soil_parameters=[1, 0.01, 1], a_s=1, b_s=0.01, c_s=1, velocity_parameters=[1, 0.01, 1],
                         a_v=1, b_v=0.01, c_v=1, maximum_iterations=1000, iteration_count=0,
                         initial_amount_of_soil=1000, p_n=0.9, p_iwd=0.9)
    result_miwd = hmw.run(G, soil_parameters=[1, 0.01, 1], a_s=1, b_s=0.01, c_s=1,
                          velocity_parameters=[1, 0.01, 1], a_v=1, b_v=0.01, c_v=1, maximum_iterations=1000,
                          iteration_count=0, initial_amount_of_soil=1000, p_n=0.9, p_iwd=0.9)

    print('Genetic algorithm                 :', result_genetic)
    print('Modified genetic algorithm        :', result_mod_genetic)
    print('Ant colony algorithm              :', result_ant)
    print('Bee colony algorithm              :', result_bee)
    print('Particle swarm optimization       :', result_pso)
    print('Intelligent water drops algorithm :', result_iwd)
    print('Modified IWD algorithm            :', result_miwd)
