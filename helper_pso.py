import libs.pso as pso
from time import time


def prepare_graph(full_g, part_g):
    graph = pso.Graph(amount_vertices=len(part_g.nodes()))
    edges = part_g.edges()
    for edge in edges:
        graph.addEdge(edge[0], edge[1], full_g.get_edge_data(edge[0], edge[1])['weight'])
        graph.addEdge(edge[1], edge[0], full_g.get_edge_data(edge[0], edge[1])['weight'])
    return graph


def add_last_node(G, solution, cost):
    prev_last_node = solution[-1]
    solution.append(solution[0])
    last_node = solution[-1]
    cost += G[prev_last_node][last_node]['weight']


def run(full_g, part_g, iterations=100, size_population=10, beta=1, alfa=0.9):
    time_start = time()
    graph = prepare_graph(full_g, part_g)

    pso_var = pso.PSO(graph, iterations=iterations, size_population=size_population, beta=beta, alfa=alfa)
    pso_var.run()

    solution = pso_var.getGBest().getPBest()
    cost = pso_var.getGBest().getCostPBest()
    add_last_node(full_g, solution, cost)

    time_stop = time()
    return {'solution': solution, 'cost': cost, 'time': time_stop - time_start}
